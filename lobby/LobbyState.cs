﻿using System;
using System.Collections.Generic;

namespace mobcas.common.lobby
{
	public class LobbyState
	{
		public enum GameTypes
		{
			Reels =   0,
			Balls =   1,
			Scratch = 2,
			PABK  =   3
		}

		public Dictionary<string, int> balance = new Dictionary<string, int> ();

		public int dkf = 100;
		public string deviceLogin = "";
		public Dictionary<string, int> jackpot = new Dictionary<string, int>();
		public int pageIndex = 0;
		public int widgetColPerPage = 4;
		public int widgetRowPerPage = 3;
		public bool notUpdated = true;
		public GameTypes GameType = GameTypes.Reels;

		// POPUP
		public bool showLoader = false;
		public bool showLoaderProgress = -1;
		public string message = "";
		public string[] messageDialogButtons = null;

		public int PageSize { get { return (widgetColPerPage * widgetRowPerPage) } }

		public LobbyState ()
		{
		}
	}
}

