﻿using System;

namespace mobcas.common.lobby
{
	public class LobbyWidget
	{

		public bool enable = true;
		public string title = "";
		public bool process = false;

		public Lobby parent;

		public virtual bool Click()
		{
			return true;
		}

		public LobbyWidget ()
		{
		}
	}
}

