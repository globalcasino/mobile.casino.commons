﻿using System;

namespace mobcas.common.lobby
{
	public class GameIcon : LobbyWidget
	{
		public string sceneName = "";

		public GameIcon (string title, string sceneName)
		{
			this.title = title;
			this.sceneName = sceneName;
		}
	}
}

