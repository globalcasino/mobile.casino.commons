﻿using System;
using System.Collections.Generic;

namespace mobcas.common.lobby
{
	public class Lobby
	{
		public List<LobbyWidget> widgets = new List<LobbyWidget>();
		public LobbyState state;

		public int PageCount {
			get {
				int count = (int)(widgets.Count / state.PageSize);
				if (widgets.Count % state.PageSize > 0)
					count += 1;
				return count;
			}
		}

		public LobbyWidget[] Page()
		{
			List<LobbyWidget> res = new List<LobbyWidget> ();
			for (int i = 0; i < state.PageSize; i++) {
				int index = (this.state.pageIndex * state.PageSize) + i;
				if (index >= widgets.Count)
					break;
				res.Add (widgets [index]);
			}
			return res;
		}

		public void NextPage()
		{
			if (state.pageIndex >= PageCount)
				state.pageIndex = 0;
			else
				state.pageIndex ++;
		}

		public void PrevPage()
		{
			if (state.pageIndex = 0)
				state.pageIndex = PageCount - 1;
			else
				state.pageIndex--;
		}


		public void ShowMessageBox(string message)
		{
			this.state.message = message;
		}

		public void ShowMessageBox(string message, params string[] buttons)
		{
			this.state.messageDialogButtons = buttons;
			ShowMessageBox (message);
		}

		public Lobby ()
		{
			this.state = new LobbyState ();
		}
	}
}

